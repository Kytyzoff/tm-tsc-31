package ru.tsc.borisyuk.tm.task;

import lombok.NonNull;
import lombok.SneakyThrows;
import ru.tsc.borisyuk.tm.component.Server;
import ru.tsc.borisyuk.tm.dto.request.AbstractRequest;
import ru.tsc.borisyuk.tm.dto.response.AbstractResponse;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

public class ServerRequestTask extends AbstractSocketServerTask {

    public ServerRequestTask(final @NonNull Server server, final @NonNull Socket socket) {
        super(server, socket);
    }

    @Override
    @SneakyThrows
    public void run() {
        @NonNull final InputStream inputStream = socket.getInputStream();
        @NonNull final ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        @NonNull final Object object = objectInputStream.readObject();
        @NonNull final AbstractRequest request = (AbstractRequest) object;
        AbstractResponse response = server.call(request);
        if (response != null) {
            @NonNull final OutputStream outputStream = socket.getOutputStream();
            @NonNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
            objectOutputStream.writeObject(response);
        }
        server.submit(new ServerRequestTask(server, socket));
    }

}