package ru.tsc.borisyuk.tm.api.component;

import ru.tsc.borisyuk.tm.dto.request.AbstractRequest;
import ru.tsc.borisyuk.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    RS execute(RQ request);

}

