package ru.tsc.borisyuk.tm.exception.user;

import ru.tsc.borisyuk.tm.exception.AbstractException;

public class EmailExistsException extends AbstractException {

    public EmailExistsException() {
        super("Error! E-mail already exists...");
    }

}
