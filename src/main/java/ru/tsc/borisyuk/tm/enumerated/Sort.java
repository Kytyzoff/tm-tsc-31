package ru.tsc.borisyuk.tm.enumerated;

import ru.tsc.borisyuk.tm.comparator.ComparatorByCreated;
import ru.tsc.borisyuk.tm.comparator.ComparatorByName;
import ru.tsc.borisyuk.tm.comparator.ComparatorByStartDate;
import ru.tsc.borisyuk.tm.comparator.ComparatorByStatus;

import java.util.Comparator;

public enum Sort {

    NAME("Sort by name", ComparatorByName.getInstance()),
    STATUS("Sort by status", ComparatorByStatus.getInstance()),
    START_DATE("Sort by start date", ComparatorByStartDate.getInstance()),
    CREATED("Sort by created", ComparatorByCreated.getInstance());

    private final String displayName;

    private final Comparator comparator;

    public String getDisplayName() {
        return displayName;
    }

    public Comparator getComparator() {
        return comparator;
    }

    Sort(String displayName, Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

}
