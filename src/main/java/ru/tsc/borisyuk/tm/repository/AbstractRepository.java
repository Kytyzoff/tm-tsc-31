package ru.tsc.borisyuk.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.borisyuk.tm.api.repository.IRepository;
import ru.tsc.borisyuk.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> entities = new ArrayList<>();

    @Override
    public void add(@NotNull final E entity) {
        entities.add(entity);
    }

    @Override
    public void addAll(@NotNull final List<E> list) {
        entities.addAll(list);
    }

    @Override
    public void remove(@NotNull final E entity) {
        entities.remove(entity);
    }

    @NotNull
    @Override
    public List<E> findAll() {
        return entities;
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final Comparator<E> comparator) {
        entities.sort(comparator);
        return entities;
    }

    @Override
    public void clear() {
        entities.removeAll(entities);
    }

    @Nullable
    @Override
    public E findById(@NotNull final String id) {
        return entities.stream()
                .filter(e -> e.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public E findByIndex(@NotNull final Integer index) {
        return entities.get(index);
    }

    @Nullable
    @Override
    public E removeById(@NotNull final String id) {
        final E entity = findById(id);
        if (entity == null) return null;
        entities.remove(entity);
        return entity;
    }

    @Nullable
    @Override
    public E removeByIndex(@NotNull final Integer index) {
        final E entity = findByIndex(index);
        if (entity == null) return null;
        entities.remove(entity);
        return entity;
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        final E entity = findById(id);
        return entity != null;
    }

    @Override
    public boolean existsByIndex(@NotNull final Integer index) {
        final E entity = findByIndex(index);
        return entity != null;
    }

}
