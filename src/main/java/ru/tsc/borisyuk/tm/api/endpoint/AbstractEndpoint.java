package ru.tsc.borisyuk.tm.api.endpoint;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.SneakyThrows;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

@Getter
@Setter
public abstract class AbstractEndpoint {

    @NonNull
    private final String host = "localhost";

    @NonNull
    private final Integer port = 6060;

    private Socket socket;

    @SneakyThrows
    protected Object call(final Object data) {
        if (data == null) return null;
        final ObjectOutputStream objectOutputStream = getObjectOutputStream();
        if (objectOutputStream == null) return null;
        objectOutputStream.writeObject(data);
        final ObjectInputStream objectInputStream = getObjectInputStream();
        if (objectInputStream == null) return null;
        return objectInputStream.readObject();
    }

    @SneakyThrows
    private ObjectOutputStream getObjectOutputStream() {
        if (socket == null) return null;
        return new ObjectOutputStream(socket.getOutputStream());
    }

    @SneakyThrows
    private ObjectInputStream getObjectInputStream() {
        if (socket == null) return null;
        return new ObjectInputStream(socket.getInputStream());
    }

    @SneakyThrows
    public void connect() {
        socket = new Socket(host, port);
    }

    @SneakyThrows
    public void disconnect() {
        socket.close();
    }

}