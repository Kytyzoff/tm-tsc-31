package ru.tsc.borisyuk.tm.exception.system;

import ru.tsc.borisyuk.tm.exception.AbstractException;

public class IndexIncorrectException extends AbstractException {

    public IndexIncorrectException() {
        super("Error. Incorrect Index name.");
    }

    public IndexIncorrectException(final Throwable cause) {
        super(cause);
    }

    public IndexIncorrectException(final String value) {
        super("Error. Value: '" + value + "' is not a number.");
    }

}
