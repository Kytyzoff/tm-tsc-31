package ru.tsc.borisyuk.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.borisyuk.tm.command.AbstractProjectCommand;
import ru.tsc.borisyuk.tm.enumerated.Role;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

public class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String name() {
        return "project-remove-by-index";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project by index...";
    }

    @Override
    public void execute() {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("[REMOVE PROJECT BY INDEX]");
        serviceLocator.getProjectService().removeByIndex(index);
        System.out.println("[OK]");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
