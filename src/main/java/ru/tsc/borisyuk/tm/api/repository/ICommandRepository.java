package ru.tsc.borisyuk.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.borisyuk.tm.command.AbstractCommand;
import ru.tsc.borisyuk.tm.model.Command;

import java.util.Collection;

public interface ICommandRepository {

    @NotNull
    AbstractCommand getCommandByName(@NotNull String name);

    @NotNull
    AbstractCommand getCommandByArg(@NotNull String arg);

    @NotNull
    Collection<AbstractCommand> getCommands();

    @NotNull
    Collection<AbstractCommand> getArguments();

    @NotNull
    Collection<String> getListCommandName();

    @NotNull
    Collection<String> getListCommandArg();

    void add(@NotNull AbstractCommand command);


}
