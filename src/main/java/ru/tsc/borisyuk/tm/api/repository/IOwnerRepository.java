package ru.tsc.borisyuk.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.borisyuk.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;

public interface IOwnerRepository<E extends AbstractOwnerEntity> extends IRepository<E> {

    void add(@NotNull String userId, @NotNull E entity);

    void remove(@NotNull String userId, @NotNull E entity);

    @NotNull
    List<E> findAll(@NotNull String userId);

    @NotNull
    List<E> findAll(@NotNull String userId, @NotNull Comparator<E> comparator);

    void clear(@NotNull String userId);

    @Nullable
    E findById(@NotNull String userId, @NotNull String id);

    @NotNull
    E findByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    E removeById(@NotNull String userId, @NotNull String id);

    @Nullable
    E removeByIndex(@NotNull String userId, @NotNull Integer index);

    boolean existsById(@NotNull String userId, @NotNull String id);

    boolean existsByIndex(@NotNull String userId, @NotNull Integer index);

}
