package ru.tsc.borisyuk.tm.api.service;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;

    public interface IPropertyService {

        @NotNull
        String getPasswordSecret();

        @NotNull
        Integer getPasswordIteration();

        @NotNull
        String getApplicationVersion();

        @NotNull
        String getAuthorName();

        @NotNull
        String getAuthorEmail();

        @NonNull
        Integer getServerPort();

    }

