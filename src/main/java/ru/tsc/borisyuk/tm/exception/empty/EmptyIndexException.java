package ru.tsc.borisyuk.tm.exception.empty;

import ru.tsc.borisyuk.tm.exception.AbstractException;

public class EmptyIndexException extends AbstractException {

    public EmptyIndexException() {
        super("Error. Index  cannot take in an empty String or null value.");
    }

}
