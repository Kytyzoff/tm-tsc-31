package ru.tsc.borisyuk.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.borisyuk.tm.api.service.IPropertyService;

public interface HashUtil {

    @NotNull String SECRET = "48";

    @NotNull Integer ITERATION = 15;

    @NotNull
    static String salt(@NotNull final IPropertyService setting, @NotNull final String value) {
        @NotNull final String secret = setting.getPasswordSecret();
        @NotNull final Integer iteration = setting.getPasswordIteration();
        return salt(secret, iteration, value);
    }

    @Nullable
    static String salt(
            @Nullable final String secret,
            @Nullable final Integer iteration,
            @Nullable final String value
    ) {
        if (value == null) return null;
        if (secret == null) return null;
        if (iteration == null) return null;
        @Nullable String result = value;
        for (int i = 0; i < iteration; i++) {
            result = md5(secret + result + secret);
        }
        return result;
    }

        @Nullable
        static String md5(@Nullable final String value) {
            if (value == null) return null;
            try {
                java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
                final byte[] array = md.digest(value.getBytes());
                final StringBuffer sb = new StringBuffer();
                for (int i = 0; i < array.length; i++)
                    sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
                return sb.toString();
            } catch (java.security.NoSuchAlgorithmException e) {
                throw new RuntimeException(e);
            }
        }

    }
