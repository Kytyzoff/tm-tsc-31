package ru.tsc.borisyuk.tm.api.endpoint;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.dto.request.ServerAboutRequest;
import ru.tsc.borisyuk.tm.dto.request.ServerVersionRequest;
import ru.tsc.borisyuk.tm.dto.response.ServerAboutResponse;
import ru.tsc.borisyuk.tm.dto.response.ServerVersionResponse;

public interface ISystemEndpoint {

    @NonNull
    ServerAboutResponse getAbout(@NonNull ServerAboutRequest request);

    @NonNull
    ServerVersionResponse getVersion(@NonNull ServerVersionRequest request);

}
